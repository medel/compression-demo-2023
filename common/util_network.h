#ifndef _NETWORK_H_
#define _NETWORK_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

struct Data
{
    face_detect_result_t face_detect_ret;
    face_landmark_result_t face_mesh_ret[MAX_FACE_NUM];
    int draw_x, draw_y, draw_w, draw_h;
    int cur_texid_mask;
};

struct addrinfo hints;
struct addrinfo *result, *rp;
int sfd = 0, s = 0;
ssize_t nread = 0;
struct sockaddr_storage peer_addr;
socklen_t peer_addr_len;

void setup_client(char *hostname)
{
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = 0;
	hints.ai_protocol = 0;

	s = getaddrinfo(hostname, "1919", &hints, &result);
	if (s != 0)
	{
	    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
	    exit(EXIT_FAILURE);
	}

	for (rp = result; rp != NULL; rp = rp->ai_next)
	{
	    sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
	    if (sfd == -1)
		continue;

	    if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
		break;

	    close(sfd);
	}

	if (rp == NULL)
	{
	    fprintf(stderr, "Could not connect\n");
	    exit(EXIT_FAILURE);
	}

	freeaddrinfo(result);
}

void setup_server()
{
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = 0;
	hints.ai_canonname = NULL;
	hints.ai_addr = NULL;
	hints.ai_next = NULL;

	s = getaddrinfo(NULL, "1919", &hints, &result);
	if (s != 0)
	{
	    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
	    exit(EXIT_FAILURE);
	}

	for (rp = result; rp != NULL; rp = rp->ai_next)
	{
	    sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
	    if (sfd == -1)
		continue;

	    if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0)
		break;			/* Success */

	    close(sfd);
	}

	if (rp == NULL)
	{
	    fprintf(stderr, "Could not bind\n");
	    exit(EXIT_FAILURE);
	}

	freeaddrinfo(result);
}

int server_read(struct Data* data_render)
{
    peer_addr_len = sizeof(struct sockaddr_storage);
    return recvfrom(sfd, data_render, sizeof(*data_render), 0, (struct sockaddr *) &peer_addr, &peer_addr_len);
}

#ifdef __cplusplus
}
#endif
#endif
